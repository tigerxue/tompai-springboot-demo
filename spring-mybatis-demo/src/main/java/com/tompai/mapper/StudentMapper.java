
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.mapper;

import java.util.List;

import com.tompai.entity.Student;

/**
 * @desc: springdemo
 * @name: StudentMapper.java
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2020年3月21日 下午8:22:16
 * @history:
 * @version: v1.0
 */

public interface StudentMapper {

	public void save(Student student);

	public Student findById(int id);
	
	public List<Student> findAll();
	
	public void modify(Student student);
	
	public void deleteById(int id);
}
