package com.tompai.mapper;

import java.util.List;

import com.tompai.entity.Student;
import com.tompai.entity.User;

public interface UserMapper {

    public void save(User record);

	public Student findById(int id);
	
	public List<Student> findAll();
	
	public void modify(User record);
	
	public void deleteById(int id);
}