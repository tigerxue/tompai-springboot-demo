
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tompai.entity.Student;
import com.tompai.mapper.StudentMapper;
import com.tompai.utils.RandomHan;

import lombok.extern.slf4j.Slf4j;

/**
 * @desc: springdemo
 * @name: StudentDemo.java
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2020年3月21日 下午10:23:41
 * @history:
 * @version: v1.0
 */
@Slf4j
public class StudentDemo {

	/**
	 * @author: tompai
	 * @createTime: 2020年3月21日 下午10:23:41
	 * @history:
	 * @param args
	 *            void
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		StudentMapper studentMapper = (StudentMapper) context.getBean("studentMapper");
		
		for (int i = 0; i < 10000; i++) {
			int len = 3;
			if (i % 7 == 0) {
				len = 2;
			}
			String name = RandomHan.randomName(true, len);
			log.info("name :" + name + " ->" + i);
			String sex = RandomUtils.nextBoolean() ? "男" : "女";
			int age = RandomUtils.nextInt(30, 100) % 100;
			Student student = new Student(name,sex,age);
			studentMapper.save(student);
		}

		/*List<Student> students = new ArrayList<Student>();
		students = studentMapper.findAll();
		for (Student student : students) {
			String string = student.toString();
			log.info("select:{}", student);
		}*/
	}

}
