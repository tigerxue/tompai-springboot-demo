package com.tompai.entity;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;

	private String name;

	private String nickname;

	private String password;

	private int age;

	private int state;

	private String salt;

	private Date inputTime;

	public int getId() {
	
		return id;
	}

	public void setId(int id) {
	
		this.id = id;
	}

	public String getName() {
	
		return name;
	}

	public void setName(String name) {
	
		this.name = name;
	}

	public String getNickname() {
	
		return nickname;
	}

	public void setNickname(String nickname) {
	
		this.nickname = nickname;
	}

	public String getPassword() {
	
		return password;
	}

	public void setPassword(String password) {
	
		this.password = password;
	}

	public int getAge() {
	
		return age;
	}

	public void setAge(int age) {
	
		this.age = age;
	}

	public int getState() {
	
		return state;
	}

	public void setState(int state) {
	
		this.state = state;
	}

	public String getSalt() {
	
		return salt;
	}

	public void setSalt(String salt) {
	
		this.salt = salt;
	}

	public Date getInputTime() {
	
		return inputTime;
	}

	public void setInputTime(Date inputTime) {
	
		this.inputTime = inputTime;
	}

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\", \"name\":\"" + name + "\", \"nickname\":\"" + nickname + "\", \"password\":\""
				+ password + "\", \"age\":\"" + age + "\", \"state\":\"" + state + "\", \"salt\":\"" + salt
				+ "\", \"inputTime\":\"" + inputTime + "\"}";
	}

}