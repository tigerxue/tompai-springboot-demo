
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.entity;

import java.util.Date;

import com.tompai.utils.DateUtil;

/**
 * @desc: springdemo
 * @name: Student.java
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2020年3月21日 下午8:14:44
 * @history:
 * @version: v1.0
 */

public class Student {

	private int id;
	private String name;
	private String sex;
	private int age;
	private Date inputIime;

	public Student() {
		super();
	}

	public Student(String name, String sex, int age) {
		super();
		this.name = name;
		this.sex = sex;
		this.age = age;
	}

	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getSex() {

		return sex;
	}

	public void setSex(String sex) {

		this.sex = sex;
	}

	public int getAge() {

		return age;
	}

	public void setAge(int age) {

		this.age = age;
	}

	public Date getInputIime() {

		return inputIime;
	}

	public void setInputIime(Date inputIime) {

		this.inputIime = inputIime;
	}

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\", \"name\":\"" + name + "\", \"sex\":\"" + sex + "\", \"age\":\"" + age
				+ "\", \"inputIime\":\"" + DateUtil.parseDateToStr(inputIime) + "\"}";
	}

}
