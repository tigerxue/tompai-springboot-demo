
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tompai.entity.User;
import com.tompai.mapper.UserMapper;
import com.tompai.utils.PinYinTool;
import com.tompai.utils.RandomHan;

import lombok.extern.slf4j.Slf4j;

/**
* @desc: spring-mybatis-demo
* @name: UserDemo.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2020年3月29日 上午10:02:16
* @history:
* @version: v1.0
*/
@Slf4j
public class UserDemo {

	/**
	* @author: tompai
	* @createTime: 2020年3月29日 上午10:02:16
	* @history:
	* @param args void
	*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		UserMapper userMapper = (UserMapper) context.getBean("userMapper");
		for (int i = 0; i < 100000; i++) {
			int len = 3;
			if (i % 3 == 0) {
				len = 2;
			}
			String name = RandomHan.randomName(true, len);
			log.info("name :" + name + " ->" + i);
			String sex = RandomUtils.nextBoolean() ? "男" : "女";
			int age = RandomUtils.nextInt(20, 70) % 100;
			User user = new User();
			user.setName(name);
			String nickname = PinYinTool.ToPinyin(name);
			user.setNickname(nickname);
			user.setAge(age);
			String password = RandomStringUtils.randomAlphanumeric(6);
			user.setPassword(password);
			int state = Integer.valueOf(RandomStringUtils.randomNumeric(1)) % 2;
			user.setState(state);
			String salt = RandomStringUtils.randomNumeric(8);
			user.setSalt(salt);
			userMapper.save(user);
		}
	}

}
